<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = ['title', 'description', 'type', 'category', 'location', 'date_created','monthly_salary_from','monthly_salary_to','year_exp'];

    public $timestamps = false;
    
    //custom timestamps name
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
}

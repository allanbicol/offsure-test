<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Jobs::orderByDesc('date_created')->get();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new Jobs();
        $job->title = $request->json()->get('title');
        $job->description = $request->json()->get('description');
        $job->type = $request->json()->get('type');
        $job->category = $request->json()->get('category');
        $job->location = $request->json()->get('location');
        $job->monthly_salary_from = $request->json()->get('salary_from');
        $job->monthly_salary_to = $request->json()->get('salary_to');
        $job->date_created = date('Y-m-d H:i:s');
        $job->year_exp = $request->json()->get('year_exp');
        $job->save();
    
        return response()->json($job,201);
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Jobs::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = Jobs::find($id);
        $job->title = $request->json()->get('title');
        $job->description = $request->json()->get('description');
        $job->type = $request->json()->get('type');
        $job->category = $request->json()->get('category');
        $job->location = $request->json()->get('location');
        $job->monthly_salary_from = $request->json()->get('salary_from');
        $job->monthly_salary_to = $request->json()->get('salary_to');
        $job->date_created = date('Y-m-d H:i:s');
        $job->year_exp = $request->json()->get('year_exp');
        $job->save();
    
        return response()->json($job,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

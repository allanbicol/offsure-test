<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Recruitment</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/template-source/fonts/icomoon/style.css')}}">

        <link rel="stylesheet" href="{{ asset('/template-source/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/bootstrap-datepicker.css')}}">
        <link rel="stylesheet" href="{{ asset('/template-source/css/animate.css')}}">

        <link rel="stylesheet" href="{{ asset('/template-source/fonts/flaticon/font/flaticon.css')}}">
    
        <link rel="stylesheet" href="{{ asset('/template-source/css/aos.css')}}">

        <link rel="stylesheet" href="{{ asset('/template-source/css/style.css')}}">

        
        {{-- <link href="{{ asset('/css/app.css')}}" rel="stylesheet" type="text/css"> --}}
        <!-- Styles -->
        
    </head>
    <body>
        <div class="site-wrap">
     
            <div id="app"></div>
            
            <script src="{{ asset('/template-source/js/jquery-3.3.1.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/jquery-migrate-3.0.1.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/jquery-ui.js')}}"></script>
            <script src="{{ asset('/template-source/js/popper.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/owl.carousel.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/jquery.stellar.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/jquery.countdown.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/jquery.magnific-popup.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/bootstrap-datepicker.min.js')}}"></script>
            <script src="{{ asset('/template-source/js/aos.js')}}"></script>
            <script src="{{ asset('/template-source/js/main.js')}}"></script>

            <script src="{{ asset('/js/app.js')}}"></script>
        </div>
    
    </body>
</html>

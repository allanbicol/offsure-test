import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Axios from 'axios';

export default class Dashboard extends Component {
    constructor()
    {
        super()
        this.state={
            jobs:[]
        }
    }

    componentDidMount()
    {
        Axios.get('/api/jobs')
        .then(res=>{
            this.setState({jobs:res.data});
        });
    }

    render() {
        return (
            <div>
               <div className="site-section bg-light">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
                                <h2 className="mb-5 h3">Jobs List</h2>
                                <Link to="/add/job" className="btn btn-primary pill py-3 px-5"><span className="icon-plus-circle"></span> Add New Job</Link>
                                <div className="blank"></div>
                                <table className="table">
                                        <thead className="bg-primary">
                                            <tr>
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>location</th>
                                                <th>Salary Range</th>
                                                <th>Date Created</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.jobs.map(job => {
                                                    return(
                                                        <tr>
                                                            <td>{job.title}</td>
                                                            <td>{job.category}</td>
                                                            <td>{job.type}</td>
                                                            <td>{job.location}</td>
                                                            <td>{job.monthly_salary_from + '-' +job.monthly_salary_to}</td>
                                                            <td>{job.date_created}</td>
                                                            <td><div className="tbl-op"><Link to={'/job-details/'+job.id} className="btn btn-sm btn-primary pill px-4 py-2">View</Link> <Link to={'/job-edit/'+job.id} className="btn btn-sm btn-info pill px-4 py-2">Edit</Link></div></td>
                                                        </tr>

                                                    )
                                                })
                                        }
                                    
                                        </tbody>
                                </table> 
                            </div>        
                        </div>             
                    </div>
                </div>
            </div>              
        );
    }
}
import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class RegisterSuccess extends Component {

    
    render() {
        return (
                
        <div className="site-section bg-light">
            <div className="container">
                <div className="row mt-10">
                    <div className="col-md-6 mb-4 mb-md-0 text-center" data-aos="fade-up" data-aos-delay="100">
                        <div className="alert alert-success" role="alert">
                            <h4 className="alert-heading">Well done!</h4>
                            <p>You are now registered to this app. Use you email and password to log in in this app. Good luck!</p>
                            <hr/>
                            <Link to="/login"><span className="btn btn-primary pill px-4 py-2">Go to login page</span></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
            
        );
    }
}
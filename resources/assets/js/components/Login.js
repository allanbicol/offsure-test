import React, { Component } from 'react';
import {login} from './UserFunctions';
import {Redirect} from 'react-router-dom';
import ErrorAlert from './ErrorAlert';
import axios from 'axios';

export default class Login extends Component {
    constructor(){
        super()
        this.state = {
            email: '',
            password: '',
            errors: {},
            alert_msg: '',
            redirect: false
        }
        
        this.onChange = this.onchange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onchange (e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit (e) {
        e.preventDefault()

        const user = {
            email: this.state.email,
            password: this.state.password
        }
        if(this.state.email && this.state.password){
            axios.post('api/login',{
                email : user.email,
                password : user.password
            },{
                headers: { 'Content-type' : 'application/json'}
            })
            .then(res => {
                sessionStorage.setItem('usertoken',res.data.token)
                window.location.href = "/";
                this.setState(redirect,true);
                console.log(res)
                 
                
            })
            .catch(err => {
                try{
                    this.setState({alert_msg:err.response.status})
                }catch(err){
                    this.setState({alert_msg: 200})
                }
                
                // console.log(err.response.status)
                
               
            })
            
            // this.props.history.push(`/home`)
            
        }
        
        
        

    }

    render() {

        

        return (
               
            <div className="site-section bg-light">
                <div className="container">
                    <div className="row mt-10 d-flex justify-content-center pb-10">
                        
                            <div className="col-md-6 mb-4 mb-md-0 bg-white" data-aos="fade-up" data-aos-delay="100">
                                <h2 className="mb-5 h3">Log in</h2>
                               {this.state.alert_msg == 400 ? <ErrorAlert message={"Invalid Credentials entered."}/>:null}
                                <div className="rounded jobs-wrap">
                                    <form noValidate onSubmit={this.onSubmit}>
                                        <div className="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" 
                                                required
                                                className="form-control" 
                                                id="exampleInputEmail1"
                                                name="email"
                                                value={this.state.email}
                                                onChange={this.onChange}
                                                aria-describedby="emailHelp" placeholder="Enter email"/>
                                           
                                        </div>
                                        <div className="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" 
                                                required
                                                className="form-control" 
                                                name="password"
                                                value={this.state.password}
                                                onChange={this.onChange}
                                                id="exampleInputPassword1" placeholder="Password"/>
                                        </div>
                                        <button type="submit" className="btn btn-lg btn-primary btn-block">Submit</button>
                                    </form>
                                    
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
                
            
        );
    }
}
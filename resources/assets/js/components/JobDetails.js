import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Moment from 'react-moment';
import {Link} from 'react-router-dom';
import Axios from 'axios';

export default class ViewJob extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            jobs:{}
        }
    }

    componentDidMount()
    {
        console.log(this.props.match.params.id);
        Axios.get('/api/jobs/'+this.props.match.params.id)
        .then(res=>{
            this.setState({jobs : res.data});
            // console.log(res.data);
        }).catch(error => console.log(error)
        );
    }

    render() {
        return (
                
            <div className="site-section bg-light">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-lg-12 mb-5">
                    
                        
                    
                        <div className="p-5 bg-white">

                        <div className="mb-4 mb-md-5 mr-5">
                        <div className="job-post-item-header d-flex align-items-center">
                            <h2 className="mr-3 text-black h4">{this.state.jobs.title}</h2>
                            <div className="badge-wrap">
                            <span className="border border-warning text-warning py-2 px-4 rounded">{this.state.jobs.type}</span>
                            </div>
                        </div>
                        <div className="job-post-item-body d-block d-md-flex">
                            <div className="mr-3"><span className="fl-bigmug-line-portfolio23"></span><span className="icon-suitcase"></span> <a href="#">{this.state.jobs.category}</a></div>
                            <div><span className="fl-bigmug-line-big104"></span> <span className="icon-room"></span><span>{this.state.jobs.location}</span></div>
                            <div> {new Intl.NumberFormat('ph-PH', { style: 'currency', currency: 'PHP' }).format(this.state.jobs.monthly_salary_from)} - {new Intl.NumberFormat('ph-PH', { style: 'currency', currency: 'PHP' }).format(this.state.jobs.monthly_salary_to)} </div>
                        </div>
                        <div><span className="icon-clock-o"></span> Posted <Moment fromNow>{this.state.jobs.date_created}</Moment></div>
                        </div>
                        <h3>Job Description</h3>
                        <p>{this.state.jobs.description}</p>

                        <p className="mt-5"><Link to="/dashboard" className="btn btn-primary  py-2 px-4">Back </Link></p>
                        </div>
                        </div>

                        
                    </div>
                </div>
            </div>
                
            
        );
    }
}
import React, { Component } from 'react';
import {BrowserRouter as Router, Link, Route,Redirect} from 'react-router-dom';
import Login from './Login';
import Register from './Register';
import RegisterSuccess from './RegisterSuccess';
import Home from './Home';
import Index from './Index';
import ViewJob from './ViewJob';
import JobDetails from './JobDetails';
import Dashboard from './Dashboard';
import AddJob from './AddJob';
import EditJob from './EditJob';

export default class Header extends Component {

    constructor(props){
        super(props)
        this.state = {
            redirect: false
        }
        this.logOut = this.logOut.bind(this);
    }

    logOut () {

        sessionStorage.setItem('usertoken','')
        sessionStorage.clear();
        this.props.history.push(`/`);
        // this.setState({redirect : true});
    }

    render() {
        const loginRegLink = (
            <ul className="site-menu js-clone-nav d-none d-lg-block">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/login"><span className="btn btn-outline-primary py-3 px-4">Log in</span></Link></li>
                <li><Link to="/register"><span className="btn btn-primary py-3 px-4">Sign up</span></Link></li>
            </ul>
        )

        const userLink = (
            <ul className="site-menu js-clone-nav d-none d-lg-block">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/Dashboard">Jobs List</Link></li>
                <li><a className="btn btn-warning py-3 px-4" href="/" onClick={this.logOut}>Logout</a></li>
            </ul>
        )
        
        if(this.state.redirect){
            return (<Redirect to={'/'}/>)
        }

        return (
            <Router>
            <div>
            <div className="site-navbar-wrap js-site-navbar bg-white">
                
                <div className="container">
                    <div className="site-navbar bg-light">
                    <div className="py-1">
                        <div className="row align-items-center">
                        <div className="col-2">
                            <h2 className="mb-0 site-logo"><Link to="/">OFFSURE<strong className="font-weight-bold">RECRUIT</strong> </Link></h2>
                        </div>
                        <div className="col-10">
                            <nav className="site-navigation text-right" role="navigation">
                            <div className="container">
                                <div className="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" className="site-menu-toggle js-menu-toggle text-black"><span className="icon-menu h3"></span></a></div>

                                {sessionStorage.getItem('usertoken') ? userLink : loginRegLink}
                                
                            </div>
                            </nav>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                
            </div>
            <Route exact path='/' component={Home} />
            <Route exact path='/home' component={Index} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/register' component={Register} />
            <Route exact path='/register-success' component={RegisterSuccess} />
            <Route exact path='/dashboard' component={Dashboard} />
            <Route exact path='/job/:id' render={props=><ViewJob{...props}/>} />
            <Route exact path='/job-edit/:id' render={props=><EditJob{...props}/>} />
            <Route exact path='/job-details/:id' render={props=><JobDetails{...props}/>} />
            <Route exact path='/add/job' component={AddJob} />
            </div>                    
               
         
            </Router>
        );
    }
}
import React, { Component } from 'react';
import {register} from './UserFunctions'

export default class Register extends Component {
    constructor(){
        super()
        this.state = {
            first_name:'',
            last_name: '',
            email: '',
            password: '',
        }
        
        this.onChange = this.onchange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onchange (e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit (e) {
        e.preventDefault()

        const newUser = {
            name: this.state.first_name + ' ' + this.state.last_name,
            email: this.state.email,
            password: this.state.password
        }
        
        register(newUser).then(res => {
            
                this.props.history.push(`/register-success`)
            
        })

    }

    render() {
        return (
    
            <div className="site-section bg-light">
            <div className="container">
                <div className="row mt-10 justify-content-center ">
                    <div className="col-md-12 mb-4 mb-md-0" data-aos="fade-up" data-aos-delay="100">
                        <h2 className="mb-5 h3">Sign up</h2>
                        <div className="rounded jobs-wrap">
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" 
                                        className="form-control" 
                                        id="first_name"  
                                        name="first_name"  
                                        value={this.state.first_name}
                                        onChange={this.onChange}
                                        placeholder="Enter First Name"/>
                                </div>
                                <div className="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" 
                                        className="form-control" 
                                        id="last_name" 
                                        name="last_name"  
                                        value={this.state.last_name}
                                        onChange={this.onChange}
                                        placeholder="Enter Last Name"/>
                                </div>

                                <div className="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" 
                                        className="form-control" 
                                        id="email" 
                                        name="email"
                                        value={this.state.email}
                                        onChange={this.onChange}
                                        aria-describedby="emailHelp" 
                                        placeholder="Enter email"/>
                                   
                                </div>
                                <div className="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" 
                                        className="form-control" 
                                        id="password" 
                                        name="password"
                                        value={this.state.password}
                                        onChange={this.onChange}
                                        placeholder="Password"/>
                                </div>
                                {/* <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                    <label className="form-check-label" for="exampleCheck1">Check me out</label>
                                </div> */}
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
              
            
        );
    }
}
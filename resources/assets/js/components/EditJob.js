import React, { Component } from 'react';
import {register} from './UserFunctions'
import SuccessAlert from './SuccessAlert';
import Axios from 'axios';

export default class EditJob extends Component {
    constructor(props){
        super(props)
        this.state = {
            id: '',
            title:'',
            description: '',
            type: '',
            category: '',
            location:'',
            salary_from:'',
            salary_to:'',
            year_exp: '',
            alert_msg:''
        }
        
        this.onChange = this.onchange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    componentDidMount()
    {
        console.log(this.props.match.params.id);
        Axios.get('/api/jobs/'+this.props.match.params.id)
        .then(res=>{
            this.setState({id : res.data.id});
            this.setState({title : res.data.title});
            this.setState({description : res.data.description});
            this.setState({type : res.data.type});
            this.setState({category : res.data.category});
            this.setState({location : res.data.location});
            this.setState({salary_from : res.data.monthly_salary_from});
            this.setState({salary_to : res.data.monthly_salary_to});
            this.setState({year_exp : res.data.year_exp});
        }).catch(error => console.log(error)
        );
    }

    onchange (e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit (e) {
        e.preventDefault()

        if(this.state.title && this.state.description){
            axios.post('/api/jobs-update/'+ this.props.match.params.id,{
                title : this.state.title,
                description : this.state.description,
                type : this.state.type,
                category : this.state.category,
                location : this.state.location,
                salary_from : this.state.salary_from,
                salary_to : this.state.salary_to,
                year_exp : this.state.year_exp,
            },{
                headers: { 'Content-type' : 'application/json'}
            })
            .then(res => {
                this.setState({alert_msg:201})
                console.log(res.response.status)
            })
            .catch(err => {
                
                
                // console.log(err.response.status)
                
               
            })
        }
        
        

    }

    render() {
        return (
    
            <div className="site-section bg-light">
            <div className="container">
                <div className="row mt-10">
                    <div className="col-md-12 mb-4 mb-md-0" data-aos="fade-up" data-aos-delay="100">
                        <h2 className="mb-5 h3">Edit Job [ID : {this.state.id}]</h2>
                        {this.state.alert_msg == 201 ? <SuccessAlert message={"Job has been successfully Updated."}/>:null}
                        <div className="rounded jobs-wrap">
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                        <label for="title">Job Title</label>
                                        <input type="text" 
                                            className="form-control" 
                                            id="title"  
                                            name="title"  
                                            value={this.state.title}
                                            onChange={this.onChange}
                                            placeholder="Enter Job Title" required/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                        <label for="description">Job Description</label>
                                        <textarea 
                                        className="form-control"
                                        name="description" 
                                        value={this.state.description}
                                        onChange={this.onChange}
                                        required>
                                        
                                        </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <label for="type">Type</label>
                                            <select 
                                            className="form-control" 
                                            name="type"
                                            value={this.state.type}
                                            onChange={this.onChange}
                                            required>
                                                <option value = ""> Select Job Type</option> 
                                                <option value = "Full Time"> Full Time</option>
                                                <option value = "Part Time"> Part Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <label for="catergory">Category</label>
                                            <select 
                                            className="form-control" 
                                            name = "category"
                                            value={this.state.category}
                                            onChange={this.onChange}
                                            >
                                                <option value = ""> Select Job Category</option>
                                                <option value = "IT"> IT</option>
                                                <option value = "Accounting"> Accounting</option>
                                                <option value = "Real Estate"> Real Estate</option>
                                                <option value = "Retail"> Retail</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" 
                                            className="form-control" 
                                            id="location"  
                                            name="location"  
                                            value={this.state.location}
                                            onChange={this.onChange}
                                            placeholder="Enter Job Location" required/>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    {/* <h3>Salary Range</h3> */}
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <label for="salary_from">Salary From</label>
                                            <input type="text" 
                                            className="form-control" 
                                            id="salary_from"  
                                            name="salary_from"  
                                            value={this.state.salary_from}
                                            onChange={this.onChange}
                                            placeholder="Min"/>
                                        
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <div className="form-group">
                                                <label for="salary_to">Salary To</label>
                                                <input type="text" 
                                                className="form-control" 
                                                id="salary_to"  
                                                name="salary_to"  
                                                value={this.state.salary_to}
                                                onChange={this.onChange}
                                                placeholder="Max"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-4">
                                        <div className="form-group">
                                        <label for="year_exp">Working Year Experience</label>
                                        <input type="text" 
                                            className="form-control" 
                                            id="year_exp"  
                                            name="year_exp"  
                                            value={this.state.year_exp}
                                            onChange={this.onChange}
                                            placeholder="Year Exp"/>
                                        </div>
                                    </div>
                                </div>
                                {this.state.alert_msg == 201 ? <SuccessAlert message={"Job has been successfully Updated."}/>:null}
                                
                               
                                <button type="submit" className="btn btn-lg btn-primary">Update</button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
              
            
        );
    }
}
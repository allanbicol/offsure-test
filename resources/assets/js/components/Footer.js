import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <div className="site-footer">
                <div className="container">
                    
                    <div className="row pt-5 mt-5 text-center">
                    <div className="col-md-12">
                        <p>
                        Copyright &copy; <script>document.write(new Date().getFullYear());</script> All Rights Reserved | Offsure Global
                    
                        </p>
                    </div>
                    
                    </div>
                </div>
            </div>
        );
    }
}
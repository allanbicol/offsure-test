import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Axios from 'axios';
import Moment from 'react-moment';

export default class Home extends Component {
    constructor()
    {
        super()
        this.state={
            jobs:[]
        }
    }

    componentDidMount()
    {
        Axios.get('/api/jobs')
        .then(res=>{
            this.setState({jobs:res.data});
        });
    }

    render() {
        return (
            <div>
                <div className="site-blocks-cover overlay inner-page bg-image"  data-aos="fade" data-stellar-background-ratio="0.5">
                    <div className="container">
                        <div className="row align-items-center justify-content-center">
                            <div className="col-md-6 text-center" data-aos="fade">
                            <h1 className="h3 mb-0">Your Dream Job</h1>
                            <p className="h3 text-white mb-5">Is Waiting For You</p>
                            {/* <p><a href="#" className="btn btn-outline-warning py-3 px-4">Find Jobs</a> <a href="#" className="btn btn-warning py-3 px-4">Apply For A Job</a></p> */}
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div className="site-section1 bg-light">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-8 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
                            <h2 className="mb-5 h3">Recent Jobs</h2>
                            <div className="rounded border jobs-wrap">
                                {
                                    this.state.jobs.map(job => {
                                        return(
                                            <div className="job-item d-block d-md-flex align-items-center  border-bottom fulltime">
                                
                                            <div className="job-details h-100">
                                            <div className="p-3 align-self-center">
                                                <h2>{job.title}</h2>
                                                <div>{job.description}</div>
                                                <div className="d-block d-lg-flex o-details">
                                                    <span className="text-info mr-3">{job.type}</span>
                                                    <div className="mr-3"><span className="icon-suitcase mr-1"></span> {job.category}</div>
                                                    <div><Moment fromNow>{job.date_created}</Moment></div>
                                                </div>
                                                <div className="d-block d-lg-flex o-details">
                                                    <div className="mr-3"><span className="icon-room mr-1"></span> {job.location}</div>
                                                    <div><span className="icon-money mr-1"></span> {job.monthly_salary_from} &mdash; {job.monthly_salary_to}</div>
                                                    <div className="job-category job-btn">
                                                    <div className="p-3">
                                                        <Link to={'/job/'+job.id}><span className="text-info p-2 rounded border border-info">Apply Now</span></Link>
                                                    </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </div>
                                            
                                        </div>
                                        )
                                    })
                                }
                            
                            </div>

                            {/* <div className="col-md-12 text-center mt-5">
                            <a href="#" className="btn btn-primary rounded py-3 px-5"><span className="icon-plus-circle"></span> Show More Jobs</a>
                            </div> */}
                        </div>
                        <div className="col-md-4 block-16" data-aos="fade-up" data-aos-delay="200">
                            <div className="d-flex mb-0">
                            <h2 className="mb-5 h3 mb-0">What's New</h2>
                            
                            </div>

                            <div className="img-bg">

                            

                            </div>

                        </div>
                        </div>
                    </div>
                </div>
             
            </div>
        );
    }
}
import axios from 'axios'

export const register = newUser => {
    return axios
        .post('api/register', newUser,{
            headers: { 'Content-type' : 'application/json'}
        })
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })
}

export const login = user => {
    return axios
        .post('api/login',{
            email : user.email,
            password : user.password
        },{
            headers: { 'Content-type' : 'application/json'}
        })
        .then(res => {
            localStorage.setItem('usertoken',res.data.token)
            localStorage.setItem('err','')
            console.log(res)
        })
        .catch(err => {
            localStorage.setItem('err',err.response.status)
            console.log(err.response.status)
        })
}

export const getProfile = () => {
    return axios
        .post('api/profile',{
            headers: { Authorization : `Bearer ${localStorage.usertoken}`}
        })
        .then(res => {
            console.log(res)
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}